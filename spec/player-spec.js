var request = require('request')
var server = require('../models/server.js')


describe('Acceptance testing /echo', function(done){
    it('Launching server...', (done) => {
        server.start(9000, done)
    })
    
    it('Getting request from echo feed', (done) => {
        request('http://localhost:9000/echo', (error, response, body) => {
            body = JSON.parse(body)
            expect(body.statusCode).toBe(200)
            expect(body.message).toBe('server is running!')
            done()
        })
    })
    
    it('Stopping server...', (done) =>{
        server.stop()
        done()
    })
})