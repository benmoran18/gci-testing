var request = require('request')
var server = require('../models/server.js')

var domain = 'http://localhost:';


describe('Acceptance testing /echo', function(done){
    it('Launching server...', (done) => {
        server.start(9000, () =>{
            domain += server.getPortNumber();
            done();
        })
    })
    
    it('Getting request from echo feed', (done) => {
        request(domain + '/echo', (error, response, body) => {
            body = JSON.parse(body)
            expect(body.statusCode).toBe(200)
            expect(body.message).toBe('server is running!')
            done()
        })
    })
    
    it('Stopping server...', (done) =>{
        server.stop()
        done()
    })
})