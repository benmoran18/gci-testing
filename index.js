"use strict"
const server = require('./models/server.js')
server.start(9000, () => {
    console.log('Server launched on port: ' + server.getPortNumber())
})