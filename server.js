"use strict"
let restify = require('restify')
const server = restify.createServer()

server.get('/echo', function(req, res, next){
    res.json({
        statusCode:200, 
        message:'server is running!'
    })
})

exports.start = (portNumber, cb) => {
    server.listen(portNumber, function(req, res){
        console.log('Server is listening on port ' + portNumber)
        if(cb != undefined) {
            cb()
        }
    });
}

exports.stop = () => {
    server.close()
}