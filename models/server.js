"use strict"
let restify = require('restify')
let portNumber = 8080;
const server = restify.createServer()

server.get('/echo', function(req, res, next){
    res.json({
        statusCode:200, 
        message:'server is running!'
    })
})

exports.start = (portNumber, cb) => {
    this.portNumber = portNumber;
    server.listen(this.portNumber, function(req, res){
        if(cb != undefined) {
            cb()
        }
    });
}

exports.getPortNumber = () => this.portNumber

exports.stop = () => server.close()